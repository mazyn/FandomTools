//See what message walls are just using the global template
//Expects a file (possible-non-template-pages.txt) with only a url on each line and a param with the template (eg `node checkForNonTemplate.js "{{w:User:Username/MessageWallGreeting}}"`)
//Outputs a list of wikis that don't use just the global template to the console
const fs = require('fs');
const got = require('grb');

require('merida').init();

//https://stackoverflow.com/a/57969193
fs.readFileSync('possible-non-template-pages.txt', 'utf8').split('\n').forEach(async url => {
    try {
        const page = await got(url + '?action=raw');
        if (page.body !== process.argv.slice(2)[0]) {
            console.log(url);
        }
    } catch (error) {
        if (error.response.statusCode === 404) {
            return; //wiki doesn't exist
        }
        console.error(error.response.statusCode + ' ' + url);
    }
});