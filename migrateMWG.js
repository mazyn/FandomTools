//Generates list of pages to move
//By Sophiedp, with help from Dorumin and BryghtShadow
//Expects a param with the wiki's url, without trailing slash or protcol, when invoked (eg, `node migrateMWG.js "name.fandom.com/fr"`)
//Outputs a migrate-MWG-results.txt that can be used for your needs, will create the file if it doesn't exist
const fs = require('fs');
const got = require('grb');

require('merida').init();

const params = {
    action: 'query',
    list: 'allpages',
    apnamespace: 1202,
    aplimit: 50,
    format: 'json'
};

const main = async params => {
    const result = await got(`https://${process.argv.slice(2)[0]}/api.php`, {
        searchParams: params
    }).json();
    let list = '';

    result.query.allpages.forEach(item => {
        const user = item.title.replace(/^.*:/, '').replace(/ /g, '_');
        list += `${item.title.replace(/ /g, '_')} User:${user}/Message_Wall_Greeting\n`;
    });

    await fs.appendFile('migrate-MWG-results.txt', list, err => {
        if (err) {
            throw err;
        }
    });

    if (result.continue) {
        const newParams = Object.assign(params, result.continue.allpages);
        main(newParams);
    }
};

main(params);