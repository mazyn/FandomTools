//Downloads all files from a wiki
//By Sophiedp with JS borrowed from Dorumin and pcj
//Expects a param with the wiki's url, without trailing slash or protcol, when invoked (eg, `node downloadWikiFiles.js "name.fandom.com/fr"`)
//Outputs a folder full of files at ./images/WIKI
const got = require('grb');
const Downloader = require('nodejs-file-downloader');

const WIKI = process.argv.slice(2)[0];

const query = ({
    params,
    onResult
}) => new Promise(async resolve => {
    const searchParams = { ...params };

    while (true) {
        const data = await got(`https://${WIKI}/api.php`, {
            searchParams
        }).json();

        onResult(data);

        if (data.continue) {
            Object.assign(
                searchParams,
                data.continue
            );
        } else {
            resolve();
            break;
        }
    }
});

(async function () {
    await query({
        params: {
            action: 'query',
            list: 'allimages',
            ailimit: 50,
            format: 'json'
        },
        onResult: data => {
            for (const page of Object.values(data.query.allimages)) {
                const downloader = new Downloader({
                    url: page.url.replace(/\/revision\/latest\?cb=.*/, ''),
                    directory: './images/' + WIKI.replace('/', '-'),
                    skipExistingFileName: true,
                    fileName: page.descriptionurl.replace(/^.*\/File:/, ''),
                    maxAttempts: 10
                });
                (async function () {
                    try {
                        await downloader.download();
                    } catch (error) {
                        console.error(page.name + ': ' + error);
                    }
                })();
            }
        }
    });
})();