//Modified version of JS orginally by Doru
(() => {
    const query = (params, cb, res) => new Promise(resolve => {
        new mw.Api().get(params).then(data => {
            cb(data);

            if (data.continue) {
                query(
                    {
                        ...params,
                        ...data.continue
                    },
                    cb,
                    resolve || res
                );
            } else {
                resolve();
            }
        });
    });

    (async () => {
        var list = [];

        await query({
            action: 'query',
            generator: 'allpages',
            gaplimit: 50,
            prop: 'revisions',
            rvprop: 'content'
        }, data => {
            for (const page of Object.values(data.query.pages)) {
                list.push(page);
            }
        });

        console.log(list.map(page => `${page.title}`).join('\n'));
    })();
})();