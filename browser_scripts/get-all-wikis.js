//Modified version of Matrix.js from dev.fandom.com by Kocka, to instead list your wikis in your console
(function () {
    'use strict';
    var config = mw.config.get([
        'wgCityId',
        'wgUserName'
    ]);
    if (config.wgCityId !== 177 || window.MatrixLoaded) {
        return;
    }
    window.MatrixLoaded = true;
    var Matrix = {
        init: function () {
            this.api = new mw.Api();
            this.spinner = '<svg class="wds-spinner wds-spinner__block" width="78" height="78" viewBox="0 0 78 78" xmlns="http://www.w3.org/2000/svg"><g transform="translate(39, 39)"><circle class="wds-spinner__stroke" fill="none" stroke-width=""stroke-dasharray="238.76104167282426" stroke-dashoffset="238.76104167282426"stroke-linecap="round" r="38"></circle></g></svg>';
            $('#my-tools-menu').append(
                $('<li>').append(
                    $('<a>', {
                        class: 'custom',
                        text: 'List my wikis'
                    }).click($.proxy(this.click, this))
                )
            );
        },
        click: function () {
            $('<div>', {
                css: {
                    background: 'rgba(var(--theme-page-background-color--rgb), 0.5)',
                    position: 'fixed',
                    height: '100%',
                    width: '100%',
                    left: '0',
                    top: '0',
                    'z-index': '1000000000'
                },
                html: this.spinner,
                id: 'matrix-spinner'
            }).appendTo(document.body);
            this.wikis = [];
            this.getWikis();
        },
        getWikis: function (offset) {
            $.get(mw.util.getUrl('Special:UserActivity', {
                uselang: 'en',
                offset: offset
            }), $.proxy(this.cbWikis, this));
        },
        cbWikis: function (data) {
            var $data = $(data);
            var nextHref = $data.find('.TablePager_nav')
                .first()
                .find('a')
                .eq(2)
                .attr('href');
            $data.find('.user-activity__table-wrapper .mw-datatable > tbody > tr').each($.proxy(function (_, row) {
                var $children = $(row).children();
                var $wiki = $children.eq(0);
                this.wikis.push($wiki.find('a').attr('href').trim());
            }, this));
            if (nextHref) {
                this.getWikis(new mw.Uri(nextHref).query.offset);
            } else {
                this.cbNirvana(this.wikis);
            }
        },
        cbNirvana: function (wikis) {
            var list = [];

            wikis.forEach(function (item) {
                list.push(item);
            });

            $('#matrix-spinner').remove();
            console.log(list.join('\n'));
        }
    };
    mw.loader.using([
        'mediawiki.api',
        'mediawiki.Uri'
    ]).then($.proxy(Matrix.init, Matrix));
})();