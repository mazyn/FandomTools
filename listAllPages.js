//Generates list of pages to move
//By Sophiedp, with help from Dorumin and BryghtShadow
//Expects a param with the wiki's url, without trailing slash or protcol, when invoked (eg, `node listAllPages.js "name.fandom.com/fr"`)
//Can also take an optional param with the namespace number desired, otherwise uses 0 (main namespace)
//Outputs a all-pages-results.txt that can be used for your needs, will create the file if it doesn't exist
const fs = require('fs');
const got = require('grb');

require('merida').init();

const params = {
    action: 'query',
    list: 'allpages',
    apnamespace: process.argv.slice(2)[1] || 0,
    aplimit: 50,
    format: 'json'
};

const main = async params => {
    const result = await got(`https://${process.argv.slice(2)[0]}/api.php`, {
        searchParams: params
    }).json();
    const list = [];

    result.query.allpages.forEach(item => {
        list.push(item.title);
    });

    await fs.appendFile('all-pages-results.txt', list.join('\n'), err => {
        if (err) {
            throw err;
        }
    });

    if (result.continue) {
        const newParams = Object.assign(params, result.continue.allpages);
        main(newParams);
    }
};

main(params);