//Gets a list of all wikis where you have a watchlist
//Expects a file called wikis.txt with each wiki url on a line (eg, `https://community.fandom.com/fr`) and two params, your username, and your watchlist token
//Outputs a list of urls to the console, where each url is a wiki that you have a watchlist on
const fs = require('fs');
const got = require('grb');

require('merida').init();

//https://stackoverflow.com/a/57969193
fs.readFileSync('wikis.txt', 'utf8').split('\n').forEach(async url => {
    try {
        const result = await got(url + '/api.php', {
            searchParams: {
                action: 'query',
                generator: 'watchlistraw',
                format: 'json',
                gwrowner: process.argv.slice(2)[0],
                gwrtoken: process.argv.slice(2)[1]
            }
        }).json();

        if (result.error) {
            if (result.error.code === 'readapidenied') {
                return console.warn('Check manually: ' + url);
            }
            return console.error(result.error);
        }
        if (!result.query) {
            return; //assume nothing in the watchlist
        }

        console.log(url);
    } catch (error) {
        if (error.response?.url.startsWith('https://community.fandom.com/wiki/Community_Central:Not_a_valid_community?from=') || error.response?.statusCode === 404 || error.response?.statusCode === 410) {
            return; //assume wiki no longer exists
        }
        console.error(error.response, url);
    }
});